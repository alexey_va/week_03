package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class ArraySimpleQueueTest {

    @Test
    public void offer() {
        ArraySimpleQueue<String> queue1 = new ArraySimpleQueue<>(10);
        for (int i = 1; i <= 10; i++) {
            assertTrue(queue1.offer("i"));
        }
        assertFalse(queue1.offer("11"));
    }

    @Test
    public void poll() {
        ArraySimpleQueue<String> queue1 = new ArraySimpleQueue<>(10);
        for (int i = 0; i < 10; i++) {
            queue1.offer(i+"");
        }
        for (int i = 0; i < 10; i++) {
            assertEquals(i +"", queue1.poll());
        }
    }

    @Test
    public void peek() {
        ArraySimpleQueue<String> queue1 = new ArraySimpleQueue<>(10);
        for (int i = 0; i < 10; i++) {
            queue1.offer(i+"");
        }
        for (int i = 0; i < 10; i++) {
            assertEquals("0", queue1.peek());
        }
    }

    @Test
    public void size() {
        ArraySimpleQueue<String> queue1 = new ArraySimpleQueue<>(10);
        for (int i = 0; i < 10; i++) {
            queue1.offer(i+"");
        }
        assertEquals(10, queue1.size());
        queue1.offer("10");
        assertEquals(10, queue1.size());
    }

    @Test
    public void capacity() {
        ArraySimpleQueue<String> queue1 = new ArraySimpleQueue<>(10);
        assertEquals(10,queue1.capacity());
    }

    @Test  (expected = IllegalArgumentException.class)
    public void pollExc() {
        ArraySimpleQueue<String> queue = new ArraySimpleQueue<>(5);
        queue.poll();
    }

    @Test
    public void pollAndOfferTest() {
        ArraySimpleQueue<String> queue1 = new ArraySimpleQueue<>(10);
        for (int i = 0; i < 10; i++) {
            assertTrue(queue1.offer(i+""));
        }
        //System.out.println(queue1.poll());
        assertEquals("0",queue1.poll());
        assertEquals(10-1,queue1.size());
        assertTrue(queue1.offer("10"));
        assertEquals("1",queue1.poll());
        assertEquals("2",queue1.poll());
        assertEquals("3",queue1.poll());
        assertEquals(10-3,queue1.size());
    }
}