package ru.edu;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LinkedSimpleListTest{

    @Test
    public void add() {
        LinkedSimpleList<String> linkedList = new LinkedSimpleList<>();

        linkedList.add("linkedItem0");
        linkedList.add("linkedItem1");
        assertEquals("linkedItem1", linkedList.get(1));
    }
    @Test
    public void set() {
        LinkedSimpleList<String> linkedList = new LinkedSimpleList<>();
        for (int i = 0; i < 10; i++){
            linkedList.add("linkedItem" + i);
        }
        linkedList.set(1,"linkedItem"+10);
        linkedList.set(0,"linkedItem"+11);

        assertEquals("linkedItem10", linkedList.get(1));
        assertEquals("linkedItem11", linkedList.get(0));
    }
    @Test
    public void get() {
        LinkedSimpleList<String> linkedList = new LinkedSimpleList<>();
        for (int i = 0; i < 10; i++){
            linkedList.add("linkedItem" + i);
        }

        assertEquals("linkedItem4",linkedList.get(4));
        assertEquals("linkedItem0",linkedList.get(0));
    }
    @Test
    public void remove() {
        LinkedSimpleList<String> linkedList = new LinkedSimpleList<>();
        for (int i = 0; i < 10; i++){
            linkedList.add("linkedItem" + i);
        }

        linkedList.remove(1);
        linkedList.remove(6);
        assertEquals("linkedItem2", linkedList.get(1));
        assertEquals("linkedItem5", linkedList.get(4));
        assertEquals("linkedItem8", linkedList.get(6));
        assertEquals(8,linkedList.size());
    }
    @Test
    public void indexOf() {
        LinkedSimpleList<String> linkedList = new LinkedSimpleList<>();
        for (int i = 0; i < 10; i++){
            linkedList.add("linkedItem" + i);
        }

        assertEquals(6, linkedList.indexOf("linkedItem6"));
        assertEquals(0, linkedList.indexOf("linkedItem0"));

        assertEquals(-1,linkedList.indexOf("Non-Elenent"));
    }

    @Test
    public void size() {
        LinkedSimpleList<String> linkedList = new LinkedSimpleList<>();
        for (int i = 0; i < 10; i++){
            linkedList.add("linkedItem" + i);
        }

        assertEquals(10,linkedList.size());
    }
}