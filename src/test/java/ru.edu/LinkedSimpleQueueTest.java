package ru.edu;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LinkedSimpleQueueTest {
    @Test
    public void offer() throws Exception {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        que.offer("queTst0");
        que.offer("queTst1");
        que.offer("queTst2");
        assertTrue(que.offer("queTst3"));
        assertEquals(4, que.size());

    }

    @Test
    public void queueTest() throws Exception {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        que.offer("queTst0");
        que.offer("queTst1");
        que.offer("queTst2");
        assertTrue(que.offer("queTst3"));
        assertEquals(4, que.size());
        que.offer("queTst4");
        assertEquals("queTst0", que.poll());
        assertEquals("queTst1", que.poll());
        assertEquals("queTst2", que.poll());
        assertEquals(2, que.size());
        que.offer("queTst5");
        que.offer("queTst6");
        assertEquals("queTst3", que.poll());
        assertEquals("queTst4", que.poll());
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void offerException() throws Exception {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        que.offer("queTst0");
        que.offer("queTst1");
        que.offer("queTst2");
        que.offer("queTst3");
        que.offer("queTst4");
        que.offer("queTst5");
    }

    @Test
    public void poll() throws Exception {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        que.offer("queTst0");
        que.offer("queTst1");
        que.offer("queTst2");
        que.offer("queTst3");

        assertEquals("queTst0", que.poll());
        assertEquals("queTst1", que.poll());
        assertEquals("queTst2", que.poll());

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void pollException() {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        que.poll();
    }

    @Test
    public void peek() throws Exception {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        que.offer("queTst0");
        que.offer("queTst1");
        assertEquals("queTst0", que.peek());
        que.offer("queTst2");
        que.offer("queTst3");
        assertEquals("queTst0", que.peek());

    }

    @Test
    public void size() throws Exception {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        que.offer("queTst0");
        que.offer("queTst1");
        assertEquals(2, que.size());
        que.offer("queTst2");
        que.offer("queTst3");
        assertEquals(4, que.size());

    }

    @Test
    public void capacity() {
        LinkedSimpleQueue<String> que = new LinkedSimpleQueue<>(5);
        assertEquals(5, que.capacity());
        LinkedSimpleQueue<String> que1 = new LinkedSimpleQueue<>(10);
        assertEquals(10, que1.capacity());

    }
}
