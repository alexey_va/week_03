package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleStackTest {

    @Test
    public void push() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        for (int i = 0; i < 15; i++){
            stack.push("test" + i);
        }
        assertEquals("test14" , stack.peek());
    }

    @Test
    public void pop() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        for (int i = 0; i < 15; i++){
            stack.push("test" + i);
        }
        stack.pop();
        assertEquals("test13" , stack.peek());
    }

    @Test
    public void peek() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        for (int i = 0; i < 15; i++){
            stack.push("test" + i);
        }
        assertEquals("test14" , stack.peek());
        assertEquals("test14" , stack.peek());

    }

    @Test
    public void empty() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        assertTrue(stack.empty());
        stack.push("test0");
        assertFalse(stack.empty());
    }

    @Test
    public void size() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        for (int i = 0; i < 10; i++){
            stack.push("test" + i);
        }
        assertEquals(10 , stack.size());
        stack.pop();
        stack.pop();
        assertEquals(8 , stack.size());
    }

    @Test
    public void capacity() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        assertEquals(15, stack.capacity());
        for (int i = 0; i < 10; i++) {
            stack.push("test" + i);
        }
        assertEquals(15, stack.capacity());
    }

    @Test (expected = IllegalArgumentException.class)
    public void popEmptyEx() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        stack.pop();
    }

    @Test
    public void isOverflow() {
        ArraySimpleStack<String> stack = new ArraySimpleStack<>();
        for (int i = 0; i < 15; i++){
            stack.push("test" + i);
        }
        assertTrue(stack.isOverflow());
    }


}
