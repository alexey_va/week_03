package ru.edu;
import org.junit.Test;

import static org.junit.Assert.*;
public class ArraySimpleListTest {

    ArraySimpleList<String> col1 = new ArraySimpleList<>();
    ArraySimpleList<int []> col2 = new ArraySimpleList<>();
    ArraySimpleList<String> col3 = new ArraySimpleList<>();
    ArraySimpleList<String> col4 = new ArraySimpleList<>();
    ArraySimpleList<String> col5 = new ArraySimpleList<>();
    ArraySimpleList<String> col6 = new ArraySimpleList<>();
    ArraySimpleList<String> col7 = new ArraySimpleList<>();


    @Test
    public void add() {
        for (int i = 0; i < 7; i++){
            col1.add("test" + i);
        }
        int [] arr = {1,2,3,4,5};
        col2.add(arr);
        assertEquals("test1", col1.get(1));
        assertEquals("test5", col1.get(5));
        assertEquals(5, (col2.get(0)[4]));
        //System.out.println("Test");
    }

    @Test
    public void set() {
        for (int i = 0; i < 6; i++){
            col3.add("test" + i);
        }
        col3.set(0,"new0");
        assertEquals("new0", col3.get(0));
    }
    @Test
    public void get() {
        for (int i = 0; i < 6; i++){
            col4.add("test" + i);
        }
        assertEquals("test0", col4.get(0));
        assertEquals("test3", col4.get(3));
    }
    @Test
    public void remove() {
        for (int i = 0; i < 6; i++){
            col5.add("test" + i);
        }
        col5.remove(4);
        assertEquals("test5", col5.get(4));
    }
    @Test
    public void indexOf() {
        for (int i = 0; i < 6; i++){
            col6.add("test" + i);
        }
        assertEquals(3,col6.indexOf("test3"));
    }
    @Test
    public void size() {
        for (int i = 0; i < 6; i++){
            col6.add("test" + i);
        }
        assertEquals(6,col6.size());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void wrongIndex() {
        for (int i = 0; i < 6; i++){
            col7.add("test" + i);
        }
        col7.remove(25);
    }
}
