package ru.edu;

public class ArraySimpleList<T> implements SimpleList<T> {
    /**
     * Начальная размерность Коллекции.
     */
    private static final int CAPACITY = 5;

    /**
     * Последний индекс массива.
     */
    private int lastIndex = 0;

    /**
     * Основной массив коллекции.
     */
    private T[] collection = (T[]) new Object[CAPACITY];

    /**
     * Проверка существования индекса.
     *
     * @param index
     */
    private void checkIndex(final int index) {
        if (index >= lastIndex || index < 0) {
            throw new IndexOutOfBoundsException("Нет такого индекса");
        }
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        collection[lastIndex] = value;
        lastIndex++;
        if (lastIndex == collection.length) {
            T[] newCollection = (T[]) new Object[lastIndex * 2];
            System.arraycopy(collection, 0, newCollection, 0, lastIndex);
            collection = newCollection;
        }
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        collection[index] = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        checkIndex(index);
        return collection[index];
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */

    @Override
    public void remove(final int index) {
        checkIndex(index);
        for (int i = index; i < lastIndex - 1; i++) {
            collection[i] = collection[i + 1];
            lastIndex--;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        int index = -1;
        for (int i = 0; i < lastIndex; i++) {
            if (value.equals(collection[i])) {
                index = i;
            }
        }
        return index;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return lastIndex;
    }
}
