package ru.edu;

public class LinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Голова очереди.
     */
    private Node<T> head = new Node<>();
    /**
     * Хвост очереди.
     */
    private Node<T> tail = new Node<>();

    /**
     * Емкость очереди.
     */
    private final int capacity;

    /**
     * Количетсво элементов в очереди.
     */
    private int size;



    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (size() == capacity()) {
            throw new IndexOutOfBoundsException("Очередь заполнена");
        }

        Node<T> newNode = new Node<>(value);

        if (head.getNext() == null) {
            setTail(newNode);
            setHead(newNode);
            tail.setPrev(getHead());
            head.setNext(getTail());

        }

        newNode.setNext(getHead());
        head.setPrev(newNode);

        setHead(newNode);

        size++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (getSize() == 0) {
            throw new IndexOutOfBoundsException("Очередь пуста");
        }

        Node<T> node = getTail();
        setTail(tail.getPrev());
        size--;
        return node.getItem();
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        return tail.getItem();
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        if (head.getNext() == null) {
            return 0;
        }
        return getSize();
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return capacity;
    }




    /**
     * Конструктор очереди с фиксированной емкостью.
     * @param cpct
     */
    public LinkedSimpleQueue(final int cpct) {
        this.capacity = cpct;
    }

    /**
     * Количество элементов в очереди.
     * @return количество элементов.
     */
    public int getSize() {
        return size;
    }

    /**
     * Возврат головы очереди.
     * @return head.
     */
    public Node<T> getHead() {
        return head;
    }

    /**
     * Присвоение головы очереди.
     * @param hd
     */
    public void setHead(final Node<T> hd) {
        this.head = hd;
    }

    /**
     * Возврат головы очереди.
     * @return tail.
     */
    public Node<T> getTail() {
        return tail;
    }

    /**
     * Присвоение хвоста очереди.
     * @param tl
     */
    public void setTail(final Node<T> tl) {
        this.tail = tl;
    }




    /**
     * Конструктор элемента очереди.
     * @param <T>
     */
    public static class Node<T> {
        /**
         * Ссылка на предыдущий элемент очереди.
         */
        private Node<T> next;
        /**
         * Объект элемента очереди.
         */
        private T item;
        /**
         * Ссылка на следующий элемент очереди.
         */
        private Node<T> prev;

        /**
         * Конструктор по умолчанию.
         */
        public Node() {
        }
        /**
         * Конструктор для элемента с новым объектом.
         * @param itm
         */
        public Node(final T itm) {
            this.item = itm;
        }

        /**
         * Возврат следующего элемента очереди.
         * @return следующий объект
         */
        public Node<T> getNext() {
            return next;
        }

        /**
         * Присвоение ссылки на следующий элемент очереди.
         * @param nxt
         */
        public void setNext(final Node<T> nxt) {
            this.next = nxt;
        }

        /**
         * Возврат объекта элемента очереди.
         * @return объект элемента очереди.
         */
        public T getItem() {
            return item;
        }

        /**
         * Возврат предыдущего элемента.
         * @return предыдущий элемент.
         */
        public Node<T> getPrev() {
            return prev;
        }

        /**
         * Присвоение ссылки на предыдущий элемент.
         * @param prv
         */
        public void setPrev(final Node<T> prv) {
            this.prev = prv;
        }
    }
}
