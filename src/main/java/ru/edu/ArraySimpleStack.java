package ru.edu;

public class ArraySimpleStack<T>  implements SimpleStack<T> {
    /**
     * Начальная размерность Коллекции.
     */
    private final int capacity = 15;

    /**
     * Последний индекс массива.
     */
    private int position = -1;

    /**
     * Контейнер элементов стэка.
     */
    private final T[] stack = (T[]) new Object[capacity];

    /**
     * Проверка на переполнение.
     * @return Возвращает булеан
     */
    public boolean isOverflow() {
        return capacity() == size();
    }

    /**
     * Добавление элемента в стэк.
     *
     * @param value элемент
     * @return true если удалось поместить элемент, false если места уже нет
     */
    @Override
    public boolean push(final T value) {
        if (isOverflow()) {
            return false;
        }
        position++;
        stack[position] = value;

        return true;    }

    /**
     * Получение и удаление элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T pop() {
        if (size() == 0) {
            throw new IllegalArgumentException("Нет элементов в стеке");
        }
        T item = stack[position];
        stack[position] = null;
        position--;
        return item;
    }

    /**
     * Получение БЕЗ удаления элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T peek() {
        return stack[position];
    }

    /**
     * Проверка пустой ли стэк.
     *
     * @return true если пустой
     */
    @Override
    public boolean empty() {
        return size() == 0;
    }

    /**
     * Количество элементов в стэке.
     *
     * @return количество
     */
    @Override
    public int size() {
        return position + 1;
    }

    /**
     * Количество элементов которое может уместиться в стэке.
     *
     * @return емкость
     */
    @Override
    public int capacity() {
        return capacity;
    }
}
