package ru.edu;

public class LinkedSimpleList<T> implements SimpleList<T> {
    /**
     * Голова коллекции.
     */
    private Element<T> head = new Element<>();;

    /**
     * Хвост коллекции.
     */
    private Element<T> last = new Element<>();

    /**
     * Размер коллекции.
     */
    private int size = 0;

    /**
     * Возврат головы списка.
     *
     * @return ссылку на head
     */
    public Element<T> getHead() {
        return head;
    }

    /**
     * Возврат хвоста списка.
     *
     * @return ссылку на last
     */
    public Element<T> getLast() {
        return last;
    }



    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        Element<T> newElement = new Element<T>(value);
        if (head.getNext() == null) {
            head.setNext(newElement);
            newElement.setPrev(head);
            newElement.setNext(last);
            last.setPrev(newElement);
        }

        Element<T> tmp = getLast().prev;
        tmp.setNext(newElement);
        newElement.setPrev(tmp);
        newElement.setNext(last);
        last.setPrev(newElement);
        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        Element<T> newElement = null;
        newElement = get12Element(index);
        newElement.setItem(value);
    }

    /**
     * Поиск элемента по половине коллекции.
     * @param index
     * @return объект поиска
     */
    private Element<T> get12Element(final int index) {
        Element<T> newElement;
        if (index < size / 2) {

            newElement = getHead();
            for (int i = 0; i <= index; i++) {
                newElement = newElement.getNext();
            }
        } else {
            newElement = getLast();
            for (int i = 0; i <= (size - 1) - index; i++) {
                newElement = newElement.getPrev();
            }

        }
        return newElement;
    }
    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        Element<T> newElement = get12Element(index);
        return newElement.getItem();    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        Element<T> newElement = get12Element(index);
        Element<T> tmpPrev = newElement.getPrev();
        tmpPrev.setNext(newElement.getNext());
        Element<T> tmpNext = newElement.getNext();
        tmpNext.setPrev(newElement.getPrev());
        size--;
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        int temp = 0;
        Element<T> newElement = getHead();

        for (int i = 0; i < size; i++) {
            newElement = newElement.getNext();
            if (value.equals(newElement.getItem())) {
                return temp;
            }
            temp++;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
















    /**
     * Внутренний класс для храения объекта.
     *
     * @param <T>
     */
    private static class Element<T> {
        /**
         * Ссылка на предыдущий элемент.
         */
        private Element<T> prev;

        /**
         * Объект.
         */
        private T item;

        /**
         * Ссылка на следующий элемент.
         */
        private Element<T> next;

        /**
         * Конструктор по умолчанию.
         */
        Element() {
            this.prev = null;
            this.item = null;
            this.next = null;
        }

        /**
         * Конструктор элемента коллекции.
         *
         * @param obj
         */
        Element(final T obj) {
            this.item = obj;
        }

        /**
         * Возврат предыдущего элемента коллекции.
         *
         * @return ссылку на предыдущий элемент
         */
        public Element<T> getPrev() {
            return prev;
        }

        /**
         * Возврат объекта элемента коллекции.
         *
         * @return ссылку на объект.
         */
        public T getItem() {
            return item;
        }

        /**
         * Возврат следующего элемента коллекции.
         *
         * @return сслыку на следующий элемент.
         */
        public Element<T> getNext() {
            return next;
        }

        /**
         * Присвоение предыдущего элемента коллекции.
         *
         * @param prv
         */
        public void setPrev(final Element<T> prv) {
            this.prev = prv;
        }

        /**
         * Присвоение объекта элемента коллекции.
         *
         * @param itm
         */
        public void setItem(final T itm) {
            this.item = itm;
        }

        /**
         * Присвоение следующего элемента коллекции.
         *
         * @param nxt
         */
        public void setNext(final Element<T> nxt) {
            this.next = nxt;
        }
    }
}
