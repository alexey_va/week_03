package ru.edu;

public class ArraySimpleQueue<T>  implements SimpleQueue<T> {
    /**
     * Начальная размерность Коллекции.
     */
    private final int capacity;

    /**
     * Последний индекс массива.
     */
    private int lastIndex;

    /**
     * Контенер элементов.
     */
    private final T[] conteiner;

    /**
     * Начала очереди.
     */
    private int head = 0;

    /**
     * Хвост очереди.
     */
    private int tail = -1;

    /**
     * Основной конструктор.
     * @param capacityTmp
     */
    public ArraySimpleQueue(final int capacityTmp) {
        this.capacity = capacityTmp;
        this.conteiner = (T[]) new Object[this.capacity];
    }

    /**
     * Проверка на переполнение.
     * @return Возвращает булеан
     */
    private boolean isOverflow() {
        return capacity() - size() == 0;
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (isOverflow()) {
            return false;
        }
        tail = (tail + 1) % capacity;
        conteiner[tail] = value;
        lastIndex++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (size() == 0) {
            throw new IllegalArgumentException("Нет элементов");
        }
        T item = conteiner[head];
        if (head == tail) {
            head = 0;
            tail = -1;
        } else {
            head = (head + 1) % capacity;
        }
        lastIndex--;
        return item;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        return conteiner[head];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return lastIndex;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return capacity;
    }
}
